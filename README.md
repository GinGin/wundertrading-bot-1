**MACD strategy with ATR**
MACD crypto trading bot from WunderTrading - [crypto automation](https://wundertrading.com/en) platform.
Check the [documentation](https://help.wundertrading.com/en) if feel the need for help.

This is a trend-based strategy that uses EMA and SMA intersection for determining the direction of the trend and MACD for the entry signal. At the same time, the strategy uses ATR, which is working as a trailing stop.